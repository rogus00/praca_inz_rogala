# praca_inzynierska_Rogala

# Calculator of phylogenetic trees
This repsoitory aims to implement methods to create phylogenetic trees. I have written three main methods: UPGMA, Neighbor Joining and Maximum Parsimony. \
It is part of my BEng thesis.

# How to use
1. Download all files
2. If needed run installation: pip install ipynb
3. Go to interface_usage file
4. Run imports
5. Run interface() function
6. Provide data
